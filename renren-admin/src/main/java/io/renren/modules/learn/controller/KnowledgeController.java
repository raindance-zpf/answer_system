package io.renren.modules.learn.controller;

import io.renren.common.utils.R;
import io.renren.modules.sys.controller.AbstractController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/learn/knowledge")
public class KnowledgeController extends AbstractController {

    @RequestMapping("/list")
    public R list ()
    {
        return R.ok();
    }
}

package io.renren.modules.learn.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import io.renren.modules.learn.entity.KnowledgeEntity;

public interface KnowledgeDao extends BaseMapper<KnowledgeEntity> {
}
